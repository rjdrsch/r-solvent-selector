% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/rank.R
\name{ssr_rank}
\alias{ssr_rank}
\alias{ssr_rank.matrix}
\alias{ssr_rank.data.frame}
\alias{ssr_rank.numeric}
\title{Rank based on minimum solubility distance}
\usage{
ssr_rank(x, ...)

\method{ssr_rank}{matrix}(x, y = NULL, n = Inf, diss = FALSE,
  hsps = c("hsp_d", "hsp_p", "hsp_hb"), x_hsps = NULL, y_hsps = NULL,
  ids = NULL, x_ids = NULL, y_ids = NULL, ...)

\method{ssr_rank}{data.frame}(x, y = NULL, n = Inf, diss = FALSE,
  hsps = c("hsp_d", "hsp_p", "hsp_hb"), x_hsps = NULL, y_hsps = NULL,
  ids = NULL, x_ids = NULL, y_ids = NULL, ...)

\method{ssr_rank}{numeric}(x, y, n = Inf, diss = FALSE,
  hsps = c("hsp_d", "hsp_p", "hsp_hb"), x_hsps = NULL, y_hsps = NULL,
  ids = NULL, x_ids = NULL, y_ids = NULL, ...)
}
\arguments{
\item{x}{An R object of matrix, or data.frame class}

\item{...}{Additional arguments to be passed to methods}

\item{y}{Null or similar object to x}

\item{n}{Number of closest \code{y} to return. The default, \code{Inf}, returns the
entire set of \code{y}, ranked for each \code{x} observation.}

\item{diss}{Logical. If \code{TRUE}, the input matrix or data frame should be
considered a dissimilarity matrix. If \code{FALSE}, the input matrix or data frame
will be converted to a dissimilarity matrix using \code{ssr_dist}}

\item{hsps}{A vector of column indexes or column names for dispersion HSP,
polar HSP, and hydrogen bonding HSP, in that order. If the column names are
not found in the input namespace, then \code{ssr_dist} checks if the input has
exactly three columns. If so, the columns are taken as "hsp_d", "hsp_p", and
"hsp_hb", in that order. If the input has more than three columns, \code{ssr_dist}
stops and throws an exception.}

\item{x_hsps}{A vector of column indexes or column names for dispersion
HSP, polar HSP, and hydrogen bonding HSP, in that order. If \code{NULL}, defaults
to the value of argument \code{hsp_cols}.}

\item{y_hsps}{A vector of column indexes or column names for dispersion
HSP, polar HSP, and hydrogen bonding HSP, in that order. If \code{NULL}, defaults
to the value of argument \code{hsp_cols}.}

\item{ids}{Optional vector of compound IDs. If passed a single string, assumes
first the value is the name of a column in the matrix or data frame. If not
found, the value is assumed to be a variable in the global environment. If
\code{length(ids) != 1}, the variable taken from the global environment. The
argument is passed to the row names of the resulting dissimilarity matrix}

\item{x_ids}{Optional vector of x IDs. If \code{NULL}, defaults to the value of
argument \code{ids}. If passed a single string, assumes first the value is the
name of a column in the matrix or data frame. If not found, the value is
assumed to be a variable in the global environment. If \code{length(x_ids) != 1},
the variable taken from the global environment. The argument is passed to the
row names of the resulting dissimilarity matrix.}

\item{y_ids}{Optional vector of y IDs. If \code{NULL}, defaults to the value of
argument \code{ids}. If passed a single string, assumes first the value is the
name of a column in the matrix or data frame. If not found, the value is
assumed to be a variable in the global environment. If \code{length(y_ids) != 1},
the variable taken from the global environment. The argument is passed to the
row names of the resulting dissimilarity matrix}
}
\value{
A data frame with the following:
\itemize{
\item x Row indexes or row names of \code{x} input
\item y Row indexes or row names of \code{y} input. If \code{y = NULL}, then this column
contains row indexes or row names of \code{x} input
\item rank Rank of \code{y} with respect to \code{x}, from lowest to highest solubility
distance
\item dist The solubility distance between \code{x} and \code{y}
}
}
\description{
Given two numeric R objects, \code{x} and \code{y}, or a solubility dissimilarity
matrix between the two (\code{ssr_dist(x, y)}), rank the closest observations of
\code{y} for each observation of \code{x}. Typically used to rank the closest \code{n}
solvents for each solute in a set.
}
\details{
\code{ssr_rank} is a generic function for matrices and data frames. One can
input a solubility dissimilarity matrix as \code{x} and setting \code{diss = TRUE}.
Alternatively, one can provide \code{x} and \code{y} objects and the function will
convert them to a solubility dissimilarity matrix using \code{ssr_dist}. In this
case, one can pass in arguments \code{hsps}, \code{x_hsps}, \code{y_hsps}, \code{ids}, \code{x_ids},
and \code{y_ids}. See \code{ssr_dist} for details.

Rankings are from smallest to largest solubility distance, and the argument
\code{n} controls the number of closest \code{y} returned for each \code{x}. To return the
entire, ranked set of \code{y}, one can pass the argument \code{n = Inf}.
}
\section{Methods (by class)}{
\itemize{
\item \code{matrix}: S3 method for class 'matrix'

\item \code{data.frame}: S3 method for class 'data.frame'

\item \code{numeric}: S3 method for class 'numeric'
}}

\examples{
set.seed(1001)
x <- matrix(sample(1:100, 9), ncol = 3)
y <- matrix(sample(1:100, 9), ncol = 3)
ssr_rank(x, y)
ssr_rank(x[1, ], y)
ssr_rank(x[1, ], y[1, ])

# Rank five closest solvents for each solute
solute_cids <- c(785, 996, 992, 289, 6914)
solute_hsps <- ssr::get_hsps(solute_cids)
solvents <- ssr::solvents
solvent_hsps <- ssr::get_hsps(solvents$cid)
ssr_rank(solute_hsps, solvent_hsps, n = 5, ids = "cid")

}
\seealso{
ssr_dist
}
