#' Hansen solubility parameters (HSPs)
#'
#' Dataset containing dispersion, polar, and hydrogen bonding Hansen solubility
#' parameters (HSPs) for various organic compounds.
#'
#' @format A data frame with variables:
#' * cid PubChem CID
#' * exptl Logical, 1 if data was experimentally-determined
#' * hsp_d Dispersion HSP in MPa^(1/2)
#' * hsp_p Polar HSP in MPa^(1/2)
#' * hsp_hb Hydrogen bonding HSP in MPa^(1/2)
#' * molar_vol Molar volume in cm^3/mol
#' @source
#' 1. Hansen, C. M. (2007). Hansen solubility parameters : a user’s
#' handbook (2nd ed.). Boca Raton : CRC Press.
#' 2. Kim S, Thiessen PA, Bolton EE, Chen J, Fu G, Gindulyte A, Han L,
#' He J, He S, Shoemaker BA, Wang J, Yu B, Zhang J, Bryant SH. PubChem Substance
#' Compound databases. Nucleic Acids Res. 2016 Jan 4; 44(D1):D1202-13. Epub 2015
#' Sep 22 \[PubMed PMID: 26400175\] doi: 10.1093/nar/gkv951.
#'
"hsps"

#' Hansen solubility parameter group contributions (HSP-GCs)
#'
#' Dataset containing group contributions (GCs) for estimating Hansen solubility
#' parameters (HSPs), as determined by Stafanis and Panyiotou (2012)
#'
#' @format A data frame with variables:
#' * id Group ID (unique)
#' * group Functional group
#' * order Indicates whether first- or second-order (see Stafanis and Panyiotou
#' (2012) for details)
#' * example Example compound containing group
#' * n_example Group count within example compound
#' * hsp_d Group contribution to dispersion HSP in MPa^(1/2)
#' * hsp_p Group contribution to polar HSP in MPa^(1/2)
#' * hsp_hb Group contribution to hydrogen bonding HSP in MPa^(1/2)
#' * hsp_p Group contribution to low-valued polar HSP (<3 MPa^(1/2))
#' * hsp_p Group contribution to low-valued hydrogen bonding HSP (<3 MPa^(1/2))
#'
#' @source
#' 1. Stefanis, E., & Panayiotou, C. (2012). A new expanded solubility
#'  paramter approach. International Journal of Pharmaceutics, 426(1–2), 29–43.
#'  https://doi.org/10.1016/j.ijpharm.2012.01.001
#'
"hsp_gcs"

#' Solvents
#'
#' Dataset containing chemical information for common solvents
#'
#' @format A data frame with variables:
#'  cid PubChem CID (unique)
#'  name Name
#'  bp Boiling point in °C
#'  solubility Water solubility in mg/l at 25°C
#'  density Density in g/ml at 20°C
#'
#' @source
#' 1. Kim S, Thiessen PA, Bolton EE, Chen J, Fu G, Gindulyte A, Han L, He J, He
#'    S, Shoemaker BA, Wang J, Yu B, Zhang J, Bryant SH. PubChem Substance and
#'    Compound databases. Nucleic Acids Res. 2016 Jan 4; 44(D1):D1202-13. Epub
#'    2015 Sep 22 \[PubMed PMID: 26400175\] doi: 10.1093/nar/gkv951
#'
"solvents"

#' Compound IDs
#'
#' Dataset containing various compound identifiers, including PubChem CIDs,
#' InChIKeys, CAS numbers, Canonical SMILES, and common names.
#'
#' @format A data frame with variables:
#' * cid PubChem CID (unique)
#' * inchikey International chemical identifier key (InChIkey)
#' * cas, CAS number
#' * smiles, Canonical SMILES
#' * name, Compound name
#'
#' @source
#' 1. Hansen, C. M. (2007). Hansen solubility parameters : a user’s
#' handbook (2nd ed.). Boca Raton : CRC Press.
#' 2. Kim S, Thiessen PA, Bolton EE, Chen J, Fu G, Gindulyte A, Han L,
#' He J, He S, Shoemaker BA, Wang J, Yu B, Zhang J, Bryant SH. PubChem Substance
#' Compound databases. Nucleic Acids Res. 2016 Jan 4; 44(D1):D1202-13. Epub 2015
#' Sep 22 \[PubMed PMID: 26400175\] doi: 10.1093/nar/gkv951.
#'
"compound_ids"

