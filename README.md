# solvselect

Optimize solvent systems to extract *multiple* organic solutes.

## Note

I'm currently rebuilding `solvselect` from scratch to coincide with the
publication of its corresponding paper. Please check back for updates soon. In the meantime, feel free to use this software package.

## Installation

Install using devtools:

```r
devtools::install_gitlab('rjdrsch/solvselect')
```
